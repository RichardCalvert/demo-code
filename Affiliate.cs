﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Newtonsoft.Json;
using System.Web.Caching;
using MyGadgetBuddy.Services;


namespace MyGadgetBuddy.Models.Entities
{


    public class Brandings
    {


        public List<Branding> BrandingList()
        {
            return mapbrandings(getBrandings());
        }

        //get Affiliates from JSON
        private Dictionary<String, Branding> getBrandings()
        {
            string file = HttpContext.Current.Server.MapPath("~/App_Data/affiliate_catalog.json");
            string Json = File.ReadAllText(file);

            Dictionary<String, Branding> retrievedBrands = JsonConvert.DeserializeObject<Dictionary<String, Branding>>(Json);

            return retrievedBrands;
        }

        //Map retrievedAffiliates to affiliate class and list
        private List<Branding> mapbrandings(Dictionary<String, Branding> Brandings)
        {

            List<DiscountCodeItem> directPromoCodes = new List<DiscountCodeItem>();

            foreach (KeyValuePair<string, Branding> availableBranding in Brandings)
            {
                if (availableBranding.Value.Name.ToLower() == "direct")
                {
                    List<DiscountCodeItem> DirectDiscounts = new List<DiscountCodeItem>();
                    DiscountCodeItems DiscountCodeItemsList = new DiscountCodeItems(availableBranding.Value.Name);
                    DirectDiscounts = DiscountCodeItemsList.GetDiscountCodeItemsData();
                    directPromoCodes = DirectDiscounts;
                    break;
                }
            }

            List<Branding> returnedBrandings = new List<Branding>();

            foreach (KeyValuePair<string, Branding> branding in Brandings)
            {
                Branding brandingToAdd = new Branding();
                brandingToAdd.PromoCodes = new List<DiscountCodeItem>();
                brandingToAdd.Name = branding.Value.Name;
                brandingToAdd.Image = branding.Value.Image;
                brandingToAdd.ShowMessage = branding.Value.ShowMessage;
                brandingToAdd.Message = branding.Value.Message;
                brandingToAdd.ShowDiscountBox = branding.Value.ShowDiscountBox;
                brandingToAdd.SetFromPromoCode = branding.Value.SetFromPromoCode;
                brandingToAdd.SetPromoFromAffiliateName = branding.Value.SetPromoFromAffiliateName;
                brandingToAdd.UseDirectPromoCodes = branding.Value.UseDirectPromoCodes;
                brandingToAdd.PresentationName = branding.Value.PresentationName;
                brandingToAdd.ShowFooterContent = branding.Value.ShowFooterContent;
                brandingToAdd.ShowIntroContent = branding.Value.ShowIntroContent;
                brandingToAdd.BrandType = branding.Value.BrandType;

                // here we need to call a method to query the discount code json and return promocode items for this brand

                if (brandingToAdd.Name.ToLower() == "direct")
                {
                    brandingToAdd.PromoCodes = directPromoCodes;
                }
                else
                {
                    DiscountCodeItems DiscountCodeItemsList = new DiscountCodeItems(brandingToAdd.Name);
                    brandingToAdd.PromoCodes = DiscountCodeItemsList.GetDiscountCodeItemsData();
                    //Not direct, but do we need to add some direct promocodes?
                    if (brandingToAdd.UseDirectPromoCodes == true)
                    {
                        brandingToAdd.PromoCodes.AddRange(directPromoCodes);
                    }
                }


                //By default we will select the first promocode as the one to use.
                //Would we always want to set a promocode though? Only if SetFromPromocode were true...

                if (brandingToAdd.SetFromPromoCode == true)
                {
                    if (brandingToAdd.PromoCodes.Count > 0)
                    {
                        brandingToAdd.DiscountCode = brandingToAdd.PromoCodes.FirstOrDefault<DiscountCodeItem>();
                    }
                }


                returnedBrandings.Add(brandingToAdd);
            }
            HttpRuntime.Cache.Insert("availableBrandings", returnedBrandings, null, DateTime.UtcNow.AddHours(12), Cache.NoSlidingExpiration);
            return returnedBrandings;
        }
    }

    //Safe to show to the general public
    public class PublicBranding
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public Guid Guid { get; set; }
        public bool ShowMessage { get; set; }
        public string Message { get; set; }
        public bool ShowDiscountBox { get; set; }
        public string PresentationName { get; set; }
        public bool ShowFooterContent { get; set; }
        public bool ShowIntroContent { get; set; }
        public string BrandType { get; set; }
    }

    //Do not show these additional details to the general public
    public class Branding : PublicBranding
    {
        public Branding()
        {
            //query the json to get info about all brandings and set them to availablebrandsings, then put that in the cache.
        }

        public Branding(string Affiliate)
        {
            SetAvailableBrandings();
            SetAffiliate(Affiliate);
        }

        public List<DiscountCodeItem> PromoCodes { get; set; }
        public DiscountCodeItem DiscountCode { get; set; }
        //public string PromoCode { get; set; }
        public bool SetFromPromoCode { get; set; }
        public bool SetPromoFromAffiliateName { get; set; }
        public bool UseDirectPromoCodes { get; set; }
        //This next member should be private - change it in future and bring all branding logic into this model
        public List<Branding> AvailableBrandings { get; set; }
        public DiscountResult DiscountResult { get; set; }

        private void SetAffiliate(string Affiliate)
        {
            foreach (Branding availableBranding in AvailableBrandings)
            {
                if (availableBranding.Name.ToLower() == Affiliate.ToLower())
                {
                    Name = availableBranding.Name;
                    Image = availableBranding.Image;
                    Guid = availableBranding.Guid;
                    ShowMessage = availableBranding.ShowMessage;
                    Message = availableBranding.Message;
                    ShowDiscountBox = availableBranding.ShowDiscountBox;
                    PromoCodes = availableBranding.PromoCodes;
                    DiscountCode = availableBranding.DiscountCode;
                    SetFromPromoCode = availableBranding.SetFromPromoCode;
                    SetPromoFromAffiliateName = availableBranding.SetPromoFromAffiliateName;
                    PresentationName = availableBranding.PresentationName;
                    ShowFooterContent = availableBranding.ShowFooterContent;
                    ShowIntroContent = availableBranding.ShowIntroContent;
                    BrandType = availableBranding.BrandType;
                    break;
                }
            }
        }

        public void CheckDiscountCode(string InputDiscountCode)
        {
            //Is the promocode legit?
            DiscountResult tempDiscountResult = new DiscountResult();
            tempDiscountResult.Success = false;
            DiscountCode = null;
            if (PromoCodes != null)
            {
                foreach (DiscountCodeItem discountItem in PromoCodes)
                {
                    if (discountItem.DiscountCode == InputDiscountCode)
                    {
                        if (discountItem.ActivationDate > DateTime.Now.Date)
                        {
                            tempDiscountResult.Message = "Code " + InputDiscountCode + " is not valid at this time.";
                        }
                        else if (discountItem.ExpiryDate < DateTime.Now.Date)
                        {
                            DiscountCode = discountItem;
                            tempDiscountResult.Message = "Code " + InputDiscountCode + " is no longer valid.";
                        }
                        else
                        {
                            DiscountCode = discountItem;
                            tempDiscountResult.Success = true;
                            tempDiscountResult.Message = "Code " + discountItem.DiscountCode + " was accepted. " + discountItem.DiscountPercent + "% discount applied.";
                        }
                        break;
                    }
                }
            }
            if (tempDiscountResult.Success == false)
            {
                //Discount invalid - set value to 0
                if (DiscountCode != null)
                {
                    DiscountCode.DiscountPercent = 0;
                }
                if (tempDiscountResult.Message == null)
                {
                    tempDiscountResult.Message = "Code " + InputDiscountCode + " not accepted, please check and try again.";
                }
            }
            DiscountResult = tempDiscountResult;
        }



        public List<Branding> GetAvailableBrandings()
        {
            List<Branding> NewAvailableBrandings = new List<Branding>();
            NewAvailableBrandings = AvailableBrandings;
            return NewAvailableBrandings;
        }

        private void SetAvailableBrandings()
        {
            List<Branding> NewAvailableBrandings = new List<Branding>();
            if (HttpRuntime.Cache.Get("availableBrandings") != null)
            {
                NewAvailableBrandings = (List<Branding>)HttpRuntime.Cache.Get("availableBrandings");
            }
            else
            {
                Brandings Brandings = new Brandings();
                NewAvailableBrandings = Brandings.BrandingList();
            }
            AvailableBrandings = NewAvailableBrandings;
        }


        public PublicBranding PublicBranding
        {
            get
            {
                return GetPublicBranding();
            }
        }



        public PublicBranding GetPublicBranding()
        {
            PublicBranding branding = new PublicBranding();

            branding.Name = Name;
            branding.Image = Image;
            branding.Guid = Guid;
            branding.ShowMessage = ShowMessage;
            branding.Message = Message;
            branding.ShowDiscountBox = ShowDiscountBox;
            branding.PresentationName = PresentationName;
            branding.ShowFooterContent = ShowFooterContent;
            branding.ShowIntroContent = ShowIntroContent;
            branding.BrandType = BrandType;

            return branding;
        }


    }


    //Instansitate this for a list of discountcodeitems for your brand (designed to work inside a brand object.)
    public class DiscountCodeItems
    {
        public DiscountCodeItems(string brandName)
        {
            //Hard to read here, but basically we are putting discountcodes (which are only specific for this brand) into DiscountCodeItemsData
            SetDiscountCodeItemsData(GetBrandDiscountCodeItems(brandName, TranslateDiscountCodeDictionaryToList(GetDiscountCodesFromDataStorage())));
        }

        private List<DiscountCodeItem> DiscountCodeItemsData { get; set; }
        public void SetDiscountCodeItemsData(List<DiscountCodeItem> DiscountCodeItems)
        {
            DiscountCodeItemsData = DiscountCodeItems;
        }
        public List<DiscountCodeItem> GetDiscountCodeItemsData()
        {
            return DiscountCodeItemsData;
        }


        private static Dictionary<string, DiscountCodeItem> GetDiscountCodesFromDataStorage()
        {
            string file = HttpContext.Current.Server.MapPath("~/App_Data/promocode_catalog.json");
            string Json = File.ReadAllText(file);
            Dictionary<string, DiscountCodeItem> gotDiscountCodes = JsonConvert.DeserializeObject<Dictionary<string, DiscountCodeItem>>(Json);
            return gotDiscountCodes;
        }


        private static List<DiscountCodeItem> TranslateDiscountCodeDictionaryToList(Dictionary<string, DiscountCodeItem> DictionaryOfCodeItems)
        {
            List<DiscountCodeItem> ReturnedDiscountCodeItems = new List<DiscountCodeItem>();
            foreach (KeyValuePair<string, DiscountCodeItem> discountcodeItem in DictionaryOfCodeItems)
            {
                DiscountCodeItem TempDiscountCodeItem = new DiscountCodeItem();
                TempDiscountCodeItem.DiscountCode = discountcodeItem.Value.DiscountCode;
                TempDiscountCodeItem.DiscountPercent = discountcodeItem.Value.DiscountPercent;
                TempDiscountCodeItem.ActivationDate = discountcodeItem.Value.ActivationDate;
                TempDiscountCodeItem.ExpiryDate = discountcodeItem.Value.ExpiryDate;
                TempDiscountCodeItem.Affiliate = discountcodeItem.Value.Affiliate;
                ReturnedDiscountCodeItems.Add(TempDiscountCodeItem);
            }

            return ReturnedDiscountCodeItems;
        }

        //Select and return only discountcodeitems for a specified brand
        public List<DiscountCodeItem> GetBrandDiscountCodeItems(string brandName, List<DiscountCodeItem> discountCodeItems)
        {
            List<DiscountCodeItem> ReturnedDiscountCodeItems = new List<DiscountCodeItem>();
            foreach (DiscountCodeItem DiscountCodeItem in discountCodeItems)
            {
                if (DiscountCodeItem.Affiliate == brandName)
                {
                    ReturnedDiscountCodeItems.Add(DiscountCodeItem);
                }
            }
            return ReturnedDiscountCodeItems;
        }


    }
}