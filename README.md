# README #

This README is for those interested in my code samples.

### What is this repository for? ###

* Some demo code
* A javascript file that takes a JSON list of gadgets and creates a form which allows the gadgets to be selected by type, then for the selected type by brand, then for the selected brand by model and finally by attibute. The function takes parameters that can pre-select any gadget from the list. See it at www.gadgetbuddy.com
* A cshtml file showing an implementation of knockout.js library - see it in action at www.gadgetbuddy.com
* A c# model showing polymorphism, constructor functions and use of a JSON file to load affiliate information.


### Who do I talk to? ###

* richcalvert0@gmail.com