﻿function activateGadgetForm() {
    //Event handlers

    $('#gadget-category').on("change", function () {
        if ($(this).val().indexOf("select") < 0) {
            gPicker.selectedType = $(this).val();
            typeSelectedShowHide();

            gPicker.selectedMake = 'unselected';
            gPicker.selectedProduct = 'unselected';
            gPicker.selectedMemory = 'unselected';
            handleMakes();
        } else {
            typeUnSelectedShowHide();
        }
        handleAddButton();
    });

    $('#gadget-make').on("change", function () {
        if ($(this).val().indexOf("select") < 0) {
            gPicker.selectedMake = $(this).val();
            makeSelectedShowHide();
            gPicker.selectedProduct = 'unselected';
            gPicker.selectedMemory = 'unselected';
            handleProducts();
        } else {
            makeUnSelectedShowHide();
        }
        handleAddButton();
    });

    $('#gadget-product').on("change", function () {
        if ($(this).val().indexOf("select") < 0) {
            gPicker.selectedProduct = $(this).val();
            productSelectedShowHide();
            gPicker.selectedMemory = 'unselected';
            handleMemory();
        } else {
            productUnSelectedShowHide();
        }
        handleAddButton();
    });

    $('#gadget-memory').on('change', function () {
        handleAddButton();
    })
}


//Object to keep gadget quoteform properties
var gPicker = {
    //Properties
    selectedType: 'unselected',
    selectedMake: 'unselected',
    selectedProduct: 'unselected',
    selectedMemory: 'unselected',
    memoryLength: 0
}


function prepareGadgetForm(formValues) {
    if (formValues != null) {
        gPicker.selectedType = formValues[0].Type;
        gPicker.selectedMake = formValues[0].Make;
        gPicker.selectedProduct = formValues[0].Model;
        gPicker.selectedMemory = formValues[0].memory;
    }
    //Getting the JSON from the gadgetscatalog
    $.getJSON('/Scripts/gadget_catalog.json', function (data) {
        gPicker.json_data = data;
        handleTypes();
    });
}


//Functions
function handleTypes() {
    //going to get unique gadget types:
    var gadgetTypes = [];
    for (var gadget in gPicker.json_data) {
        gadgetTypes.push(gPicker.json_data[gadget].gadgetCategory);
    }
    var uniqueTypes = gadgetTypes.filter(function (item, i, ar) { return ar.indexOf(item) === i; }).sort();
    if (gPicker.selectedType == 'unselected') {
        var gadgetTypeOptions = "<option value='unselected' selected='selected'>Select gadget</option>";
        for (var j = 0; j < uniqueTypes.length; j++) {
            gadgetTypeOptions += "<option value='" + uniqueTypes[j] + "'>" + uniqueTypes[j] + "</option>";
        }

        $('#gadget-category').empty();
        $('#gadget-make').empty();
        $('#gadget-product').empty();
        $('#gadget-memory').empty();
        $('#gadget-category').html(gadgetTypeOptions);
    } else {
        var gadgetTypeOptions = "<option value='unselected' >Select gadget</option>";
        for (var j = 0; j < uniqueTypes.length; j++) {
            if (uniqueTypes[j] == gPicker.selectedType) {
                gadgetTypeOptions += "<option value='" + uniqueTypes[j] + "' selected='selected'>" + uniqueTypes[j] + "</option>";
            } else {
                gadgetTypeOptions += "<option value='" + uniqueTypes[j] + "'>" + uniqueTypes[j] + "</option>";
            }
        }
        $('#gadget-category').empty();
        $('#gadget-make').empty();
        $('#gadget-product').empty();
        $('#gadget-memory').empty();
        $('#gadget-category').html(gadgetTypeOptions);
        handleAddButton();
        typeSelectedShowHide();
        handleMakes();
    }


}

function handleMakes() {
    //going to get unique gadget makes for the selected type
    var gadgetMakes = [];
    for (var gadget in gPicker.json_data) {
        if (gPicker.json_data[gadget].gadgetCategory == gPicker.selectedType) {
            gadgetMakes.push(gPicker.json_data[gadget].make);
        }
    }
    var uniqueMakes = gadgetMakes.filter(function (item, i, ar) { return ar.indexOf(item) === i; }).sort();
    if (gPicker.selectedMake == 'unselected') {
        var gadgetMakeOptions = "<option value='unselected' selected='selected'>Select make</option>";

        for (var j = 0; j < uniqueMakes.length; j++) {
            gadgetMakeOptions += "<option value='" + uniqueMakes[j] + "'>" + uniqueMakes[j] + "</option>";
        }

        $('#gadget-make').empty();
        $('#gadget-product').empty();
        $('#gadget-memory').empty();
        $('#gadget-make').html(gadgetMakeOptions);
    } else {
        var gadgetMakeOptions = "<option value='unselected'>Select make</option>";

        for (var j = 0; j < uniqueMakes.length; j++) {
            if (gPicker.selectedMake == uniqueMakes[j]) {
                gadgetMakeOptions += "<option value='" + uniqueMakes[j] + "' selected='selected'>" + uniqueMakes[j] + "</option>";
            } else {
                gadgetMakeOptions += "<option value='" + uniqueMakes[j] + "'>" + uniqueMakes[j] + "</option>";
            }
        }

        $('#gadget-make').empty();
        $('#gadget-product').empty();
        $('#gadget-memory').empty();
        $('#gadget-make').html(gadgetMakeOptions);
        handleAddButton();
        makeSelectedShowHide();
        handleProducts();
    }
}

function handleProducts() {
    //going to get unique gadget products for the selected type and make
    var gadgetProducts = [];
    for (var gadget in gPicker.json_data) {
        if (gPicker.json_data[gadget].make == gPicker.selectedMake && gPicker.json_data[gadget].gadgetCategory == gPicker.selectedType) {//All products of the selected make and type
            gadgetProducts.push(gPicker.json_data[gadget].model);
        }
    }
    var uniqueProducts = gadgetProducts.filter(function (item, i, ar) { return ar.indexOf(item) === i; }).sort();

    if (gPicker.selectedProduct == 'unselected') {
        var gadgetProductOptions = "<option value='unselected' selected='selected'>Select product</option>";
        for (var j = 0; j < uniqueProducts.length; j++) {
            gadgetProductOptions += "<option value='" + uniqueProducts[j] + "'>" + uniqueProducts[j] + "</option>";
        }

        $('#gadget-product').empty();
        $('#gadget-memory').empty();
        $('#gadget-product').html(gadgetProductOptions);
    } else {
        var gadgetProductOptions = "<option value='unselected' '>Select product</option>";
        for (var j = 0; j < uniqueProducts.length; j++) {
            if (gPicker.selectedProduct == uniqueProducts[j]) {
                gadgetProductOptions += "<option value='" + uniqueProducts[j] + "' selected='selected'>" + uniqueProducts[j] + "</option>";
            } else {
                gadgetProductOptions += "<option value='" + uniqueProducts[j] + "'>" + uniqueProducts[j] + "</option>";
            }
        }

        $('#gadget-product').empty();
        $('#gadget-memory').empty();
        $('#gadget-product').html(gadgetProductOptions);
        handleAddButton();
        productSelectedShowHide();
        handleMemory();
    }
}


function handleMemory() {
    //going to get unique gadget products for the selected type and make
    gPicker.memoryLength = 0;
    var gadgetMemory = [];
    for (var gadget in gPicker.json_data) {
        if (gPicker.json_data[gadget].make == gPicker.selectedMake && gPicker.json_data[gadget].gadgetCategory == gPicker.selectedType && gPicker.json_data[gadget].model == gPicker.selectedProduct) {//All memory options of the selected make and type and product
            if (gPicker.json_data[gadget].memory != "") {
                gadgetMemory.push(gPicker.json_data[gadget].memory);
            }
        }
    }

    gadgetMemory = gadgetMemory.sort(function (a, b) {
        return +/\d+/.exec(a)[0] - +/\d+/.exec(b)[0];
    });

    if (gadgetMemory.length > 0) {
        gPicker.memoryLength = gadgetMemory.length;
        var uniqueMemory = gadgetMemory.filter(function (item, i, ar) { return ar.indexOf(item) === i; });
        var gadgetMemoryOptions = "<option value='unselected' selected='selected'>Select memory</option>";

        for (var j = 0; j < uniqueMemory.length; j++) {
            gadgetMemoryOptions += "<option value='" + uniqueMemory[j] + "'>" + uniqueMemory[j] + "</option>";
        }

        $('#gadget-memory').empty();
        $('#gadget-memory').html(gadgetMemoryOptions);
        if ($('.gadget-memory-group').hasClass("hidden")) {
            $('.gadget-memory-group').removeClass("hidden");
        }
    } else {//no memory options
        $('#gadget-memory').empty();
    }
}


function handleAddButton() {
    var addGadgetButton = $('#AddGadget');
    if (hasAValue($('#gadget-category'))) {
        if (hasAValue($('#gadget-make'))) {
            if (hasAValue($('#gadget-product'))) {
                if (gPicker.memoryLength > 0) {
                    if (hasAValue($('#gadget-memory'))) {
                        setButtonToEnabled(addGadgetButton);
                    } else {
                        setButtonToDisabled(addGadgetButton)
                    }
                } else {
                    setButtonToEnabled(addGadgetButton);
                }
            } else {
                setButtonToDisabled(addGadgetButton)
            }
        } else {
            setButtonToDisabled(addGadgetButton)
        }
    } else {
        setButtonToDisabled(addGadgetButton)
    }
}

function setButtonToDisabled(addGadgetButton) {
    if (!addGadgetButton.attr("disabled")) {
        addGadgetButton.attr("disabled", true)
    };
}

function setButtonToEnabled(addGadgetButton) {
    if (addGadgetButton.attr("disabled")) {
        addGadgetButton.removeAttr("disabled")
    };
}

function hasAValue(element) {
    if (element.val() != null) {
        elementValue = element.val().toLowerCase()

        if (elementValue.indexOf('select') < 0) {
            //then value is not 'select' or 'unselected'
            if (elementValue != '') {
                //then value is neither 'select' or '' - it must be some value.
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }

}



//Some show / hide elements functionality

function typeSelectedShowHide() {
    if ($('.gadget-make-group').hasClass("hidden")) {
        $('.gadget-make-group').removeClass("hidden");
    }
    if (!$('.gadget-product-group').hasClass("hidden")) {
        $('.gadget-product-group').addClass("hidden");
    }
    if (!$('.gadget-memory-group').hasClass("hidden")) {
        $('.gadget-memory-group').addClass("hidden");
    }
}

function typeUnSelectedShowHide() {
    if (!$('.gadget-make-group').hasClass("hidden")) {
        $('.gadget-make-group').addClass("hidden");
    }
}

function makeSelectedShowHide() {
    if ($('.gadget-product-group').hasClass("hidden")) {
        $('.gadget-product-group').removeClass("hidden");
    }
    if (!$('.gadget-memory-group').hasClass("hidden")) {
        $('.gadget-memory-group').addClass("hidden");
    }
}

function makeUnSelectedShowHide() {
    if (!$('.gadget-product-group').hasClass("hidden")) {
        $('.gadget-product-group').addClass("hidden");
    }
}

function productSelectedShowHide() {
    if ($('.gadget-memory-group').hasClass("hidden")) {
        //$('.gadget-memory-group').removeClass("hidden");
    }
}

function productUnSelectedShowHide() {
    if (!$('.gadget-memory-group').hasClass("hidden")) {
        $('.gadget-memory-group').addClass("hidden");
    }
}